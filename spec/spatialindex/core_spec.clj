(ns spatialindex.core-spec
  (:require [speclj.core :refer :all]
            [cheshire.core :as json]
            [spatialindex.core :as index]
            ))


(describe "find-document-bbox"

  (it "should return a seq of two points"
      (let [bbox (index/find-document-bbox (json/parse-string (slurp "test/fixtures/simple.json") true))]
        (should== 2 (count bbox)))))

