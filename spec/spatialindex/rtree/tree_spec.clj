(ns spatialindex.rtree.tree-spec
  (:require [speclj.core :refer :all]
            [cheshire.core :as json]
            [spatialindex.core :refer :all]
            [spatialindex.rtree.tree :refer :all]

            ))

(def box1 [[0 0][1 2]])
(def box2 [[0 1][2 4]])
(def box3 [[2 2][3 6]])
(def entry1 {:_id (gensym) :box box1})
(def entry2 {:_id (gensym) :box box2})
(def node1 {:entries [entry1] :box box1})
(def tree {:root node1})

(describe "choose-leaf"
  (it "should return a leaf into which to insert this entry "
      (let [a (choose-leaf tree entry2)] 
        (should= (:enl a) 2))))

(describe "simple insert"
  (it "should insert a record into a non-overflowing node"
     (let [a (insert-entry tree node1 entry2)]
        (should= (count (:entries a)) 2))))

