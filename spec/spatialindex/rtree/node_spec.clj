(ns spatialindex.rtree.node-spec
  (:require [speclj.core :refer :all]
            [cheshire.core :as json]
            [spatialindex.core :refer :all]
            [spatialindex.rtree.node :refer :all]
            ))

(def box1 [[0 0][1 2]])
(def box2 [[0 1][2 4]])
(def box3 [[2 2][3 6]])
(def entry1 {:_id (gensym) :box box1})
(def entry2 {:_id (gensym) :box box2})
(def node1 {:entries [entry1] :box box1})

(describe "not-leaf-node"
  (it "should deny this is a leaf node "
      (let [a (is-leaf node1)] 
        (should= a false))))
