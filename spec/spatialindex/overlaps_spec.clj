(ns spatialindex.overlaps-spec
  (:require [speclj.core :refer :all]
            [cheshire.core :as json]
            [spatialindex.overlaps :refer :all]
            ))

(def box0 [[0 0][1 1]])
(def box1 [[0 0][1 2]])
(def box2 [[2 2][3 4]])
(def box3 [[2 3][6 7]])

(describe "find-overlaps-yes"
  (it "should return true as boxes overlap"
      (let [a (overlaps box2 box3)] 
        (should= a true))))

(describe "find-overlaps-no"
  (it "should return false as boxes dont overlap"
      (let [a (overlaps box0 box3)] 
        (should= a false))))

(describe "overlap-group-yes"
  (it "should return true as the box overlaps"
     (let [a (overlaps-with box0 (vector box1 box2 box3))]
        (should= a true))))

(describe "overlap-group-no"
  (it "should return false as the box not overlaps"
     (let [a (overlaps-with (vector box0 box2 box3))]
        (should= a false))))
