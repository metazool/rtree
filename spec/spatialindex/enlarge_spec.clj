(ns spatialindex.enlarge-spec
  (:require [speclj.core :refer :all]
            [cheshire.core :as json]
            [spatialindex.enlarge :refer :all]
            [spatialindex.core :refer :all]
            ))

(def box1 [[0 0][1 2]])
(def box2 [[0 1][2 4]])
(def box3 [[2 2][3 6]])

(describe "find-area"
  (it "should return area of two"
      (let [a (area box1)] 
        (should= a 2))))

(describe "find-enlargement"
  (it "should find the enlargement needed to make A B, where A and B are pairs of [[minx maxx] [miny maxy]] rectangles"
     (let [e (enlargement box1 box2)]
        (should= e 6))))  

(describe "least-enlargement" 
  (it "should find the bbox with least enlargement"
    (let [box (least-enlargement {:box box1} (vector {:box box2} {:box box3}))]
      (println box)
      (should= (:enl box) 6))))

