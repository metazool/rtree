# spatialindex 

This is an attempt at a basic implementation
of standalone spatial indexing in clojure,
starting with R-trees.

Follow the original algorithm here:
http://postgis.org/support/rtree.pdf

This was inspired and suggested by 
Howard Butler and his work in python based on 
libspatialindex. 

## Usage


## License

Copyright © 2014 Jo Walsh 

Distributed freely using the Artistic License
