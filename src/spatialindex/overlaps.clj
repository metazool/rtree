(ns spatialindex.overlaps
     (:require [spatialindex.core :refer :all]))

(defn above
   "Returns true if box2 is completely above box1"
   [box1 box2]
   ; if box2 minimum-Y > box1 maximum-Y
   (if (> ((box2 0) 1) ((box1 1) 1))
     true
     false))

(defn below [box1 box2]
   (if (< ((box2 1) 1) ((box1 0) 1))
     true
     false))

(defn left [box1 box2]
   (if (< ((box2 1) 0) ((box1 0) 0))
     true
     false))

(defn right [box1 box2]
   (if (> ((box2 0) 0) ((box1 1) 0))
     true
     false))

(defn overlaps 
  "Returns true if box1 overlaps box2"
  [box1 box2]
  ; if completely above, below, left or right
  ; then there must be no overlap
  (cond 
    (above box1 box2) false
    (below box1 box2) false
    (right box1 box2) false
    (left box1 box2) false
    :else true))
  
(defn overlaps-with
   "Returns true if box1 overlaps one of boxes"
   [box1 & [boxes]]
   (def laps (map #(overlaps box1 %) boxes))
     (if (some #{true} laps)
       true
       false))
  
