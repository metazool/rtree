(ns spatialindex.enlarge
     (:require [spatialindex.core :refer :all]))

(defn enlargement
  "Smallest difference between box 1 and 2"
  [box enlargebox]
  (let [coords (find-bbox [box enlargebox])]
  (def expanded (area coords))
  (def original (area box))
  (- expanded original)))

(defn wrap-enlargement
  "Like enlargement but returns map with box"
  [entry enlarge-entry]
  (let [enlarge (enlargement (:box entry) (:box enlarge-entry))]
    (assoc enlarge-entry :enl enlarge)
  ))   

(defn smallest-area
   "returns map with smallest area"
   [boxes]
   (first (sort (map #(area (:box %)) boxes))))

(defn least-enlargement
  [entry & [entries]]
  
  (def enlargements (map #(wrap-enlargement entry %) entries))
  (def least (first (sort (map #(:enl %) enlargements))))
  (let [least-enlarged (filter #(= (:enl %) least) enlargements)]
    ;(println least-enlarged)
    (if (= 1 (count least-enlarged)) 
      (first least-enlarged)
      (smallest-area least-enlarged))))
  
  
