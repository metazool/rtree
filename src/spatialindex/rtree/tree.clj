(ns spatialindex.rtree.tree
  (:require [spatialindex.enlarge :refer :all] 
            [spatialindex.rtree.node :refer :all]))

(defn find-least-enlargement [node entry]
    (let [enlarge (least-enlargement entry (:entries node))]
       (println enlarge)
       enlarge
))

(defn choose-leaf [tree entry]
   (let [node (:root tree)]
      (if (is-leaf node) 
         node
         (do (find-least-enlargement node entry))))) 

(defn insert-entry
  [tree node entry]
  (if (< (count (node :entries)) max-node-entries )
    (do (assoc node :entries (conj (node :entries) entry)))
    (do
      (let [leaf (choose-leaf tree entry)]
        (assoc leaf :entries (conj (node :entries) entry))))))	    
         







