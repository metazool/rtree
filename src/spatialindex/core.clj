(ns spatialindex.core
  (:gen-class))

(defn width
  "Width of a box"
  [box]
  (- (get (box 1) 0) (get (box 0) 0)))

(defn height
  "Height of a box"
  [box]
  (- ((box 1) 1) ((box 0) 1)))

(defn perimeter
  "Perimeter of a box"
  [box]
  (def x (width box))
  (def y (height box))
  (+ x x y y))

(defn area
  "Area of the [[minx miny][maxx maxy]] bounding box"
  [box]
  (def x (width box))
  (def y (height box ))
  (* x y))

(defn get-coords 
  "docstring"
  [feature]
  (let [coords (get-in feature [:geometry :coordinates])]
  (get coords 0)))

(defn find-bbox
  "Where [coords] is a seq or seq of seqs of points"
  [coordlist]
  (def coords (reduce into coordlist))  
  (def lons (map (fn [c] (get c 0)) coords))
  (def lats (map (fn [c] (get c 1)) coords))

  (def minx (first (sort lats)))
  (def miny (first (sort lons)))
  (def maxx (last (sort lats)))
  (def maxy (last (sort lons)))
  [[minx miny] [maxx maxy]])


(defn find-document-bbox
  "Expects a GeoJSON doc through cheshire's json/parse_string"
; USAGE:   (find-bbox (json/parse-string (slurp "example.json") true))
  [doc]
  (let [coordlist (map get-coords (doc :features))]
   (find-bbox coordlist)))


(defn map->extents
   "Turns a map of min/max/x/y params into coordinate extents"
   [bbox]
   (vector [[(bbox :minx) (bbox :miny)]
             [(bbox :minx) (bbox :maxy)]
             [(bbox :maxx) (bbox :maxy)]
             [(bbox :maxx) (bbox :miny)]
             [(bbox :minx) (bbox :miny)]]))

(defn document-extents 
   [doc]     
   (def bbox (find-document-bbox doc))
   (map->extents bbox))

