(defproject spatialindex "0.0.1"
  :description "Spatial indexes including R-tree"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.5.1"]
                [meridian/clj-jts "0.0.2"]
                [cheshire "5.3.1"]
                [meridian/shapes-impl "0.0.2"]]
  :profiles {:dev {:dependencies [[speclj "2.5.0"]]}}

  :plugins [[speclj "2.5.0"]]

  :test-paths ["spec"])

